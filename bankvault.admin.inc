<?php
/**
 * @file
 * BankVault integration module admin settings.
 */

/**
 * Form API Callback - display system settings form.
 */
function bankvault_admin_form($form, &$form_state) {
  $settings = _bankvault_get_settings();

  if (empty($settings['enabled'])) {
    $form['bankvault_testing'] = [
      '#markup' => t('<strong>Currently in testing mode.</strong><br />
        To access the login forms for testing go to
        <a href="/user/login/bankvault">user/login/bankvault</a> or
        place the "BankVault: User Login Testing" block on a page.'),
    ];
  }

  $form['bankvault_api_key'] = [
    '#title' => t('API Key'),
    '#description' => t('Your API key for the BankVault service.'),
    '#type' => 'textfield',
    '#default_value' => $settings['api_key'],
    '#description' => t('Other settings on this form will be disabled until this is entered.')
  ];

  $form['bankvault_enabled'] = [
    '#type' => 'checkbox',
    '#return_value' => 1,
    '#default_value' => $settings['enabled'],
    '#disabled' => empty($settings['api_key']),
    '#title' => t('Show QR code on user login forms'),
    '#prefix' => t('<label>Enable BankVault</label>'),
    '#description' => t('When ticked the QR code will appear on the live login forms. Otherwise the module will be in test mode.')
  ];

  $form['bankvault_position'] = [
    '#type' => 'radios',
    '#options' => [
      'right' => 'Right',
      'underneath' => 'Underneath',
    ],
    '#default_value' => $settings['position'],
    '#disabled' => empty($settings['api_key']),
    '#title' => t('Where should the QR code be located on the login page?'),
   ];

  $form['bankvault_forms'] = [
    '#type' => 'checkboxes',
    '#options' => [
      'user_login' => 'Login Page',
      'user_login_block' => 'Login Block',
    ],
    '#default_value' => $settings['forms'],
    '#disabled' => empty($settings['api_key']),
    '#title' => t('Which forms should the QR code be displayed on?'),
   ];

  return system_settings_form($form);

}