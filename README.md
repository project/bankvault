This module adds a high security login option to your site.

MasterKey meets the most advanced standards for Zero-Trust networks and Zero-Trust devices.

Scan the QR code and use your mobile phone to securely enter your login credentials.