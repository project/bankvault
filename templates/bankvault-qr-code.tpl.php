<div class="bankvault-wrapper <?php echo $position; ?>">
  <div class="bankvault-separator">
    <div class="line"></div>
    <div class="text">OR</div>
    <div class="line"></div>
  </div>

  <div class="bankvault-qr-code-wrapper">
    <h3>Scan to Login</h3>
    <div class="bankvault-qr-code">
      <img src="<?php echo $qr_code; ?>" alt="QR Code Login" />
    </div>
    <div class="bankvault-credit">
      <div>Secured By</div>
      <img src="<?php echo $logo; ?>" alt="Secured by BankVault" />
    </div>
  </div>
</div>
